from flask import Flask, request, send_file
from PIL import Image, ImageDraw, ImageFont
import io

app = Flask(__name__)


@app.route("/")
def home():
    # Get the IP address of the user. Support proxies, mostly for Ngrok
    ip_addr = request.headers.get("X-Forwarded-For", request.remote_addr)

    # Create a new image with a white background
    image = Image.open("./is_this_your_ip.jpg")

    # Draw the IP address and user agent on the image
    font = ImageFont.truetype("FreeSansOblique.ttf", 50)
    draw = ImageDraw.Draw(image)
    draw.text((780, 200), ip_addr, fill=(255, 255, 255), font=font)

    # Save the image to a BytesIO object
    img_io = io.BytesIO()
    image.save(img_io, "JPEG", quality=70)
    img_io.seek(0)

    # Return the image as a response
    return send_file(img_io, mimetype="image/jpeg")


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
